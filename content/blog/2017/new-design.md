+++
title = "New design"
categories = [ "meta" ]
tags = [ "design", "web", "meta" ]
date = "2017-03-10"
draft = true
+++

Welcome to my new home!

My previous website, although probably fancier and more *material-esque*, was based
on [Harp](https://harpjs.com/) with a custom made theme built with [Material
Design Lite](http://getmdl.io/).

This previous solution wasn't very flexible, because Harp isn't particularly
designed for building a blog, but only sets of static pages. Each time you write
a new article, you have to remember to [update the list of
posts](https://harpjs.com/recipes/blog-posts-list). This may become tedious on
the long run.

Since I am person who likes experimenting, discovering, and breaking things, I
decided it was time to start a new adventure!

<div class="text-center">
<img alt="Rebuild all the things!" src="/img/blog/2017/rebuild-all-things.jpg">
</div>

## Go Hugo, Go!

I discovered [Hugo](http://gohugo.io/) by browsing the endless list of all the
existing [static web site generators](https://www.staticgen.com/). It's one of
the firsts on the list! Between all the cool features it brings to its users, I
personally like:

* [themes](http://themes.gohugo.io/)
* handles markdown and html flawlessly
* autogeneration of sitemaps

I've based my theme on [Goa](https://github.com/shenoybr/hugo-goa), naming it
*Yugort*. That's not probably the best name to come up with, but it was the best
I could think of!
