+++
title = "LVM Snapshots and system upgrades"
categories = [ "lvm", "guides", "linux" ]
tags = [ "lvm", "guides", "linux", "ubuntu" ]
date = "2017-04-14"
draft = true
+++

[LVM](https://wiki.ubuntu.com/Lvm) is a powerful tool and way to manage your
partition on your GNU/Linux installation, both on server and PCs.

I find very convenient to manage my partitions using LVM, especially when
dealing with distro updates.

With the following configuration:
```
$ lsblk
NAME                        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                           8:0    0 489.1G  0 disk
├─sda2                        8:2    0     1G  0 part /boot
├─sda3                        8:3    0 477.6G  0 part
│ ├─ubuntu-home             253:1    0   260G  0 lvm  /home
│ ├─ubuntu-swap             253:2    0     2G  0 lvm  [SWAP]
│ └─ubuntu-root             253:3    0    30G  0 lvm  /
└─sda1                        8:1    0   512M  0 part /boot/efi
```

We run the following command to create a snapshot of the partition `ubuntu-root`
with size `20G`. I chose to size my snapshot this way because the original partition
is `30G`, but only `21G` of `30G` are in use, and usually updates don't change
everything and/or get usually bigger than existing installations.

```
sudo lvcreate -s -n root-backup -L 20G ubuntu/root
```

We should now have a extra logical volume, strictly linked to its parent,
`ubuntu/root` volume.

```
$ sudo lvdisplay ubuntu/root-backup
  --- Logical volume ---
  LV Path                /dev/ubuntu/root-backup
  LV Name                root-backup
  VG Name                ubuntu
  LV UUID                ZHdKWV-Z69b-CPIM-39sE-gr1G-2Ho9-vr6JU2
  LV Write Access        read/write
  LV Creation host, time viserion, 2017-04-14 16:41:56 +0200
  LV snapshot status     active destination for root
  LV Status              available
  # open                 0
  LV Size                30.00 GiB
  Current LE             7680
  COW-table size         20.00 GiB
  COW-table LE           5120
  Allocated to snapshot  7.44%
  Snapshot chunk size    4.00 KiB
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:5
```

This partition can be mounted and browsed like any other partition. Any
modification performed will be saved (snapshots are copy-on-write, only
different inodes get persisted, others are hard-linked to originals).

Now, we proceed by upgrading the system. Hopefully everything should go
flawlessly, but in case anything goes wrong, we can restore the partition to the
previous snapshot state with the following command:

```
$ sudo lvconvert --merge /dev/ubuntu/root-backup
```

In case the origin logical volume is active, merging will occur on the next
reboot. (Merging can be done even from a LiveCD). After the merge, the snapshot
partition will cease to exist.

If the upgrade procedures succeeds, we can later delete the snapshot, by typing:
```
$ sudo lvremove ubuntu/root-backup
```
