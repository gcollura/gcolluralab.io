+++
title       = "Talks"
description = "The complete list of all my talks"
categories  = [ "talks" ]
tags        = []
keywords    = "giulio,collura,gcollura,web,developer,student,talks,ubuntu"
date        = "2017-03-10"
slug        = "talks"
draft       = true
singlepage  = true
+++

### Ubuntu Touch: Sviluppo App e Convergenza
Apr 26, 2014  
Pordenone Fiera del Radioamatore 2014, Italy  
[Slides](http://www.slideshare.net/giuliocollura/ubuntu-touch-sviluppo-app-e-convergenza)

<hr/>

### Ubuntu Touch: Sviluppo App e Convergenza (2)
Sep 28, 2014  
Todi Appy Days 2014, Italy  
[Slides](http://www.slideshare.net/giuliocollura/ubuntu-touch-sviluppo-app-e-convergenza-39669271)

<hr/>

### Ubuntu Phone: the community smartphone
Nov 28, 2014  
Codemotion Milan 2014, Italy  
[Slides](http://www.slideshare.net/giuliocollura/ubuntu-phone-the-community-smartphone)
[Info](http://milan2014.codemotionworld.com/speaker/887/)

<hr/>

### Ubuntu: all you need is Qt-ness
Mar 13, 2015  
QtDay 2015 Florence, Italy  
[Slides](http://slidr.io/gcollura/ubuntu-all-you-need-is-qt-ness)
[Info](http://www.qtday.it/events/ubuntu-need-qt-ness-riccardo-padovani-e-giulio-collura/)

<hr/>

### Ubuntu Phone: the community smartphone
Apr 25, 2015  
Fiera del Radioamatore 2015 Pordenone, Italy  
[Slides](http://slidr.io/gcollura/ubuntu-phone-the-community-smartphone)

<hr/>

### Ubuntu Phone: collaborare alle Core Apps
Apr 26, 2015  
Fiera del Radioamatore 2015 Pordenone, Italy  
[Slides](http://slidr.io/gcollura/ubuntu-phone-collaborare-alle-core-apps)
