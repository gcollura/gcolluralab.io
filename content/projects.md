+++
title      = "Projects"
categories = [ "projects" ]
tags       = [ "projects", "playground" ]
date       = "2017-03-10"
slug       = "projects"
draft      = true
singlepage = true
+++

**[www.gcollura.com](https://gitlab.com/gcollura/gcollura.gitlab.io)** -- Built
with opensource technologies and lots of love.

**[playground](https://github.com/gcollura/playground)** -- Solutions to coding
challenges. Programming is fun!

**[SaucyBacon](https://github.com/gcollura/saucybacon)** -- A simple and fast
recipe manager, mobile application developed in QtQuick and C++ for the Ubuntu
platform. Ubuntu App Showdown Winner (Sept 2013). Abandoned.

**[Slingshot](https://launchpad.net/slingshot)** -- Default app launcher for
Elementary OS, now maintained by the Elementary OS community
