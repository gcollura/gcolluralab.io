+++
title      = "About me"
categories = [ "about" ]
tags       = [ "resume", "cv", "bio" ]
date       = "2017-03-10"
slug       = "about-me"
draft      = false
singlepage = true
+++

Hi, my name's Giulio!

I love spending my time programming, hacking stuff and contributing to open
source projects. I like listening to music, riding my bicycle and playing soccer
with my friends, in my spare time.

<!-- ## Short Resumé -->
<!---->
<!-- ### Education -->
<!---->
<!-- **M.S. in Data Science and Engineering**, *EURECOM Télécom ParisTech*, France   -->
<!-- Oct 2016 -- Current   -->
<!-- Data Science and Machine Learning specialization. Expected graduation in March -->
<!-- 2018. Coursework: *machine learning, neural networks and deep learning, data -->
<!-- mining, big data, statistical inference, semantic web, system security, -->
<!-- cryptography, project management* -->
<!---->
<!-- **M.S. in Computer Software Engineering**, *Politecnico di Torino*, Italy   -->
<!-- Oct 2015 -- Current   -->
<!-- Coursework: *software engineering, computer networks, system programming, web -->
<!-- programming* -->
<!---->
<!-- **B.S. in Computer Engineering**, *Politecnico di Torino*, Italy   -->
<!-- Oct 2012 -- Oct 2015   -->
<!-- Coursework: *algorithms, data structures, distributed databases, computer -->
<!-- architectures, object oriented programming* -->
<!---->
<!-- ### Work experience -->
<!---->
<!-- **Web Engineer**, *Zero11 S.r.l.*, Turin -- Italy   -->
<!-- Jul 2015 -- Sept 2016   -->
<!-- Software engineer in many web oriented solutions, such as e-commerce, CRM, -->
<!-- CMS, web management apps and RESTful API integrations. Engineered, delivered -->
<!-- and maintained front-end and back-end solutions in J2EE, PHP and Javascript. -->
<!---->
<!-- **Software Engineer Intern**, *SynArea S.r.l.*, Turin -- Italy   -->
<!-- Apr -- Jul 2015   -->
<!-- Development of a Web 3D solution with a client-server architecture based on -->
<!-- modern web technologies, such as Node.js, Python and WebGL/Three.js. -->
<!-- Implemented an algorithm to dynamically handle loading and distribution of -->
<!-- different level of details of the environment to efficiently render the scene -->
<!-- with good performance, even on mobile devices. -->
